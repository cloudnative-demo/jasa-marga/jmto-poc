<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Images;
use App\Services\GoogleStorageService as GoogleStorageService;

class ImageController extends Controller
{
    private $googleStorageService;
    public function __construct(GoogleStorageService $googleStorageService){
        $this->googleStorageService = $googleStorageService;
    }

    public function index()
    {
        return view('imageUpload');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.$request->image->extension();

        // simpan ke folder /storage/app/public/image
        $request->image->storeAs('images', $imageName);
        // insert ke postgre
        $image = Images::create(['tittle' => $request->tittle, 'description' => $request->description, 'image' => $imageName]);
        /*
            Write Code Here for
            Store $imageName name in DATABASE from HERE
        */

        $result = $this->googleStorageService->uploadFile($imageName);

        $gcp_url = $result['GCP_URL'];

        Images::where('id', $image->id)
            ->update(['gcp_url' => $gcp_url]);

        return back()->with('success','You have successfully upload image')
            ->with('image',$imageName);

    }


    public function show($id)
    {
        //
    }

 
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
