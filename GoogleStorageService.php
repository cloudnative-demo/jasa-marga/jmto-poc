<?php
namespace App\Services;

use Google\Cloud\Storage\StorageClient as StorageClient;
use Illuminate\Support\Facades\Storage as Storage;

class GoogleStorageService{
    private $storageClientService;
    private $storageFacade;
    public function __construct(StorageClient $storageClient, Storage $storage){
        $this->storageClientService = $storageClient;
        $this->storageFacade = $storage;
    }

    public function uploadFile(String $fileName){
        $fileSource = $this->storageFacade::path('images/'.$fileName);
        $googleConfigFile = file_get_contents(config_path('gcs-credentials.json'));
        $storageBucketName = config('googlecloud.storage_bucket');

        // init google storace client
        $storage = new StorageClient([
            'keyFile' => json_decode($googleConfigFile, true)
        ]);
        // preparing bucket
        $bucket = $storage->bucket('test-storage-kbumn');

        // upload to bucket
        $bucket->upload(fopen($fileSource, 'r'),
            [
                'name' => $fileName
            ]
        );

        return [
            'GCP_URL' => config('googlecloud.storage_url').'/'.config('googlecloud.storage_bucket').'/'.$fileName
        ];
    }
}
