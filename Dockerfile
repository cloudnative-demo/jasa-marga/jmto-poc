
FROM php:8.1-fpm


RUN whoami
RUN php -v
RUN cat /etc/*release
RUN cat /etc/passwd


# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpq5 \
    libpq-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    libonig-dev \
    libxml2-dev \
    libmemcached-dev \
    lua-zlib-dev \
    locales \
    jpegoptim optipng pngquant gifsicle \
    git \
    curl \
    iputils-ping \
    nginx \
    zip \
    # postgresql-dev \
    postgresql postgresql-contrib \
    traceroute \
    unzip

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install composer
RUN curl -sS https://getcomposer.org/installer​ | php -- --install-dir=/usr/local/bin --filename=composer


# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer


# Install php extensions
RUN docker-php-ext-install pdo sockets bcmath mbstring pdo_pgsql pgsql exif pcntl gd


# Set working directory
WORKDIR /var/www


# Copy code to /var/www
COPY --chown=www-data:root . /var/www


# PHP Error Log Files
RUN mkdir /var/log/php
RUN touch /var/log/php/errors.log && chmod 777 /var/log/php/errors.log


# Deployment steps
RUN composer install --optimize-autoloader --no-dev
RUN php artisan key:generate
RUN composer dump-autoload
RUN php artisan config:clear
RUN php artisan config:cache
RUN composer require google/cloud-storage --with-all-dependencies

RUN chown -R www-data /var/www


USER www-data

CMD php artisan serve --host=0.0.0.0 --port=8080
EXPOSE 8080
